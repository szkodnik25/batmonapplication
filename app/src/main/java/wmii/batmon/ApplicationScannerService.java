package wmii.batmon;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.List;

import wmii.applicationscanner.Application;
import wmii.applicationscanner.CacheHandler;
import wmii.applicationscanner.DataUsageManager;

import static android.widget.Toast.LENGTH_SHORT;


/**
 * Usługa skanująca zużycie danych w tle.
 * Created by Mateusz on 2015-11-20.
 */
public class ApplicationScannerService extends Service {

    public static final int REFRESH_RATE = 10000;

    private Handler handler = null;
    private Runnable runnableScanner = null;

    private List<Application> apps;
    private Gson gson;
    private CacheHandler cacheHandler;
    private TypeToken<List<Application>> applicationsListTypeToken = new TypeToken<List<Application>>(){};

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        Toast.makeText(this, "Usługa skanująca uruchomiona.", LENGTH_SHORT).show();

        handler = new Handler();
        runnableScanner = initializeRunnableScanner();

        handler.postDelayed(runnableScanner, REFRESH_RATE);
        gson = new Gson();
        cacheHandler = new CacheHandler(this);
    }

    private Runnable initializeRunnableScanner() {
        return new Runnable() {
            @Override
            public void run() {
                String json = cacheHandler.read();
                apps = gson.fromJson(json, applicationsListTypeToken.getType());

                for (Application app : apps) {
                    try {
                        long mobileRxFg = DataUsageManager.getMobileRxForegroundData(app.getUid());
                        long mobileRxBg = DataUsageManager.getMobileRxBackgroundData(app.getUid());

                        long wlanRxFg = DataUsageManager.getWlanRxForegroundData(app.getUid());
                        long wlanRxBg = DataUsageManager.getWlanRxBackgroundData(app.getUid());

                        if (wlanRxBg == 0L && wlanRxFg == 0L && mobileRxBg == 0L && mobileRxFg == 0L) {
                            continue;
                        }

                        if (app.getMobileRxFg() > mobileRxFg) {
                            app.setMobileRxFg(app.getMobileRxFg() + mobileRxFg);
                        } else app.setMobileRxFg(mobileRxFg);

                        if (app.getMobileRxBg() > mobileRxBg) {
                            app.setMobileRxBg(app.getMobileRxBg() + mobileRxBg);
                        } else app.setMobileRxBg(mobileRxBg);

                        if (app.getWlanRxFg() > wlanRxFg) {
                            app.setWlanRxFg(app.getWlanRxFg() + wlanRxFg);
                        } else app.setWlanRxFg(wlanRxFg);

                        if (app.getWlanRxBg() > wlanRxBg) {
                            app.setWlanRxBg(app.getWlanRxBg() + wlanRxBg);
                        } else app.setWlanRxBg(wlanRxBg);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                json = gson.toJson(apps);
                cacheHandler.write(json);
                handler.postDelayed(runnableScanner, REFRESH_RATE);
            }
        };
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnableScanner);
        Toast.makeText(this, "Usługa skanująca zatrzymana.", LENGTH_SHORT).show();
    }
}
