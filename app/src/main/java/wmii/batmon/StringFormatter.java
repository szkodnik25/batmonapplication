package wmii.batmon;

public class StringFormatter {

    public static String formatLongToStringWithDataUnit(long number) {
        if (number < (1024 * 1024)) {
            double kb = (double) (number / 1024);
            return String.valueOf(kb + "Kb");
        }

        double mb = (double) (number / (1024 * 1024));
        return String.valueOf(mb + "Mb");
    }
}
