package wmii.batmon.online;

import java.util.List;

import wmii.applicationscanner.Application;

public interface OnlineManager {

    String URL = "http://192.168.0.55:8080/";

    List<OnlineApplication> getOnlineApplications();
    OnlineApplication getOnlineApplication(String _id);

    void sendApplication(Application application);
    void sendApplications(List<Application> applications);

    void updateApplication(Application application);
    void updateUsageIfNecessary(Application application, long mobileRxFg, long mobileRxBg, long wlanRxFg, long wlanRxBg);
}
