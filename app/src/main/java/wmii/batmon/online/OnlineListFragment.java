package wmii.batmon.online;

import android.app.ListFragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import wmii.applicationscanner.Application;
import wmii.applicationscanner.CacheHandler;
import wmii.batmon.R;
import wmii.batmon.preferences.PreferencesManager;
import wmii.batmon.preferences.SharedPreferencesManager;

public class OnlineListFragment extends ListFragment {

    PreferencesManager preferencesManager;
    OnlineManager onlineManager;
    OnlineListAdapter onlineListAdapter;

    TypeToken<List<Application>> applicationsListTypeToken;
    CacheHandler cacheHandler;
    Gson gson;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        new OnlineApplicationsLoader().execute();
        return inflater.inflate(R.layout.list_view_fragment, container, false);
    }

    private class OnlineApplicationsLoader extends AsyncTask<Void, Void, List<OnlineApplication>> {

        ProgressDialog progressDialog = null;

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(getActivity(), "Wczytywanie", "Wczytuję aplikacje online...");
            preferencesManager = new SharedPreferencesManager(getActivity());
            preferencesManager.initializePreferences();
            onlineManager = new OnlineManagerImpl(preferencesManager.getWholeServerAddres());

            cacheHandler = new CacheHandler(getActivity());
            gson = new Gson();
            applicationsListTypeToken = new TypeToken<List<Application>>(){};
            super.onPreExecute();
        }

        @Override
        protected List<OnlineApplication> doInBackground(Void... params) {
            if (cacheHandler.doesCacheFileExist()) {
                List<Application> applications = gson.fromJson(cacheHandler.read(), applicationsListTypeToken.getType());
                List<Application> applicationsToSend = new ArrayList<>();
                for (Application application : applications) {
                    if (!application.hasNullDataUsage()) {
                        applicationsToSend.add(application);
                    }
                }
                onlineManager.sendApplications(applicationsToSend);
            }
            return onlineManager.getOnlineApplications();
        }

        @Override
        protected void onPostExecute(List<OnlineApplication> onlineApplications) {
            if (onlineApplications != null) {
                Collections.sort(onlineApplications);
                onlineListAdapter = new OnlineListAdapter(getActivity(), R.layout.online_list_element, onlineApplications);
                setListAdapter(onlineListAdapter);
            }
            progressDialog.dismiss();
        }
    }
}
