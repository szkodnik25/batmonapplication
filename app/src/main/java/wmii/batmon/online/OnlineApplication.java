package wmii.batmon.online;

import java.util.List;

import wmii.applicationscanner.Application;

public class OnlineApplication extends Application {
    int num;

    public OnlineApplication(int uid, String name, String _id, List<String> permissions, int num) {
        super(uid, name, _id, permissions);
        this.num = num;
    }

    public OnlineApplication(Application application, int num) {
        super(0, application.getName(), application.get_id(), application.getPermissions());
        this.num = num;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
