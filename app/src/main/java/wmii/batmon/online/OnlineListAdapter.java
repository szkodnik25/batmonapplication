package wmii.batmon.online;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import wmii.batmon.R;
import wmii.batmon.StringFormatter;

public class OnlineListAdapter extends ArrayAdapter<OnlineApplication> {

    private List<OnlineApplication> onlineApplications;
    private Context context;

    public OnlineListAdapter(Context context, int resource, List<OnlineApplication> objects) {
        super(context, resource, objects);
        this.onlineApplications = objects;
        this.context = context;
    }

    @Override
    public int getCount() {
        return onlineApplications.size();
    }

    @Override
    public OnlineApplication getItem(int position) {
        return onlineApplications.get(position);
    }

    @Override
    public int getPosition(OnlineApplication item) {
        return onlineApplications.indexOf(item);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final OnlineViewHolder onlineViewHolder;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.online_list_element, null);
            onlineViewHolder = new OnlineViewHolder();

            onlineViewHolder.onlineApplicationNameTextView = (TextView) convertView.findViewById(R.id.online_app_name_and_number);
            onlineViewHolder.wlanIcon = (ImageView) convertView.findViewById(R.id.online_wlan_icon);
            onlineViewHolder.mobileIcon = (ImageView) convertView.findViewById(R.id.online_mobile_icon);

            Drawable wlanIcon = ContextCompat.getDrawable(context, R.drawable.ic_device_signal_wifi_3_bar);
            Drawable mobileIcon = ContextCompat.getDrawable(context, R.drawable.ic_device_network_cell);

            onlineViewHolder.wlanIcon.setImageDrawable(wlanIcon);
            onlineViewHolder.mobileIcon.setImageDrawable(mobileIcon);
            onlineViewHolder.onlineMobileTextView = (TextView) convertView.findViewById(R.id.online_mobile_data_usage);
            onlineViewHolder.onlineWlanTextView = (TextView) convertView.findViewById(R.id.online_wlan_data_usage);

            convertView.setTag(onlineViewHolder);
        } else {
            onlineViewHolder = (OnlineViewHolder) convertView.getTag();
        }

        OnlineApplication onlineApplication = onlineApplications.get(position);
        onlineViewHolder.onlineApplicationNameTextView.setText(String.format("%s (%d)", onlineApplication.getName(), onlineApplication.getNum()));

        String mobileUsage = String.format("FG: %s, BG: %s",    StringFormatter.formatLongToStringWithDataUnit(onlineApplication.getMobileRxFg()),
                                                                StringFormatter.formatLongToStringWithDataUnit(onlineApplication.getMobileRxBg()));
        onlineViewHolder.onlineMobileTextView.setText(mobileUsage);

        String wlanUsage = String.format("FG: %s, BG: %s",  StringFormatter.formatLongToStringWithDataUnit(onlineApplication.getWlanRxFg()),
                                                            StringFormatter.formatLongToStringWithDataUnit(onlineApplication.getWlanRxBg()));
        onlineViewHolder.onlineWlanTextView.setText(wlanUsage);

        return convertView;
    }

    private class OnlineViewHolder {
        private TextView onlineApplicationNameTextView;
        private ImageView wlanIcon;
        private ImageView mobileIcon;
        private TextView onlineMobileTextView;
        private TextView onlineWlanTextView;
    }
}
