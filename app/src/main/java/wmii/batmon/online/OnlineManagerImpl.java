package wmii.batmon.online;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import wmii.applicationscanner.Application;
import wmii.restclient.BatMonRestClient;

public class OnlineManagerImpl implements OnlineManager {

    BatMonRestClient batMonRestClient;
    String url;
    Gson gson;
    TypeToken<List<OnlineApplication>> onlineApplicationTypeToken = new TypeToken<List<OnlineApplication>>(){};

    public OnlineManagerImpl(String url) {
        this.url = url;
        batMonRestClient = new BatMonRestClient(url);
        gson = new Gson();
    }

    @Override
    public List<OnlineApplication> getOnlineApplications() {
        String response = batMonRestClient.GET().replace("u'", "'");
        return gson.fromJson(response, onlineApplicationTypeToken.getType());
    }

    @Override
    public OnlineApplication getOnlineApplication(String _id) {
        String response = batMonRestClient.GET(_id).replace("u'", "'");
        return gson.fromJson(response, OnlineApplication.class);
    }

    @Override
    public void sendApplication(Application application) {
        batMonRestClient.POST(application);
    }

    @Override
    public void sendApplications(List<Application> applications) {
        batMonRestClient.POST(applications);
    }

    @Override
    public void updateApplication(Application application) {
        batMonRestClient.PUT(application);
    }

    @Override
    public void updateUsageIfNecessary(Application application, long mobileRxFg, long mobileRxBg, long wlanRxFg, long wlanRxBg) {
        long newMobileRxFg = 0L;
        long newMobileRxBg = 0L;
        long newWlanRxFg = 0L;
        long newWlanRxBg = 0L;

        newMobileRxFg = calculateDifference(application.getMobileRxFg(), mobileRxFg);
        newMobileRxBg = calculateDifference(application.getMobileRxBg(), mobileRxBg);
        newWlanRxFg = calculateDifference(application.getWlanRxFg(), wlanRxFg);
        newWlanRxBg = calculateDifference(application.getWlanRxFg(), wlanRxBg);

        batMonRestClient.PUT(application.get_id(), newMobileRxFg, newMobileRxBg, newWlanRxFg, newWlanRxBg);
    }

    private long calculateDifference(long applicationData, long newData) {
        if (newData < applicationData) {
            return newData;
        }

        return newData - applicationData;
    }
}
