package wmii.batmon.applicationinfo;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import wmii.applicationscanner.Application;
import wmii.batmon.R;
import wmii.batmon.StringFormatter;

public class ApplicationInfoFragment extends Fragment {

    public static final String KEY = "application";

    public static ApplicationInfoFragment newInstance(Application application) {
        ApplicationInfoFragment fragment = new ApplicationInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY, application);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Application application = (Application) getArguments().getSerializable(KEY);
        View view = inflater.inflate(R.layout.application_info_fragment, container, false);

        return new ApplicationInfoViewBuilder(view, application).initView();
    }

    private class ApplicationInfoViewBuilder {

        View view;
        Application application;

        List<String> permissions;
        ArrayAdapter<String> listViewAdapter;

        PermissionsListBuilder permissionsListBuilder;

        public ApplicationInfoViewBuilder(View view, Application application) {
            this.view = view;
            this.application = application;
            this.permissions = new ArrayList<>();
            permissionsListBuilder = new PermissionsListBuilderImpl(getActivity());
        }

        public View initView() {
            view.setClickable(true);
            initHeader();
            initIcon();
            initUsage();
            initPermissionsListView();
            initShowAllPermissionsCheckBox();
            return view;
        }

        private void initHeader() {
            TextView name = (TextView) view.findViewById(R.id.name_text_view);
            TextView packageName = (TextView) view.findViewById(R.id.package_text_view);

            name.setText(application.getName());
            packageName.setText(application.get_id());
        }

        private void initIcon() {
            ImageView icon = (ImageView) view.findViewById(R.id.app_icon_info_fragment);
            icon.setClickable(true);

            try {
                icon.setImageDrawable(getActivity().getPackageManager().getApplicationIcon(application.get_id()));
            } catch (PackageManager.NameNotFoundException e) {
                Log.d("Name not found", e.toString());
            }
        }

        private void initUsage() {
            TextView foregroundMobileTextView = (TextView) view.findViewById(R.id.mobile_foreground);
            TextView backgroundMobileTextView = (TextView) view.findViewById(R.id.mobile_background);

            TextView foregroundWlanTextView = (TextView) view.findViewById(R.id.wlan_foreground);
            TextView backgroundWlanTextView = (TextView) view.findViewById(R.id.wlan_background);

            foregroundMobileTextView.setText(StringFormatter.formatLongToStringWithDataUnit(application.getMobileRxFg()));
            backgroundMobileTextView.setText(StringFormatter.formatLongToStringWithDataUnit(application.getMobileRxBg()));

            foregroundWlanTextView.setText(StringFormatter.formatLongToStringWithDataUnit(application.getWlanRxFg()));
            backgroundWlanTextView.setText(StringFormatter.formatLongToStringWithDataUnit(application.getWlanRxBg()));
        }
        
        private void initPermissionsListView() {
            permissions.addAll(permissionsListBuilder.buildCommonPermissionsList(application));
            this.listViewAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, permissions);
            ListView permissionsListView = (ListView) view.findViewById(R.id.permissions_list_view);
            permissionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    for (Map.Entry<String, PermissionWithDescription> entry : permissionsListBuilder.getPermissionsHashMap().entrySet()) {
                        if (entry.getValue().getName().equals(listViewAdapter.getItem(position))) {
                            showDialogIfAvailable(entry.getKey());
                        }
                    }
                }
            });
            permissionsListView.setAdapter(listViewAdapter);
        }

        private void initShowAllPermissionsCheckBox() {
            CheckBox showAllCheckBox = (CheckBox) view.findViewById(R.id.show_all_permissions_checkbox);
            showAllCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        permissions.clear();
                        permissions.addAll(permissionsListBuilder.buildAllPermissionsList(application));
                        listViewAdapter.notifyDataSetChanged();
                    } else {
                        permissions.clear();
                        permissions.addAll(permissionsListBuilder.buildCommonPermissionsList(application));
                        listViewAdapter.notifyDataSetChanged();
                    }
                }
            });
        }

        private void showDialogIfAvailable(String permission) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(permissionsListBuilder.getPermissionsHashMap().get(permission).getDescription());
            builder.setTitle(permissionsListBuilder.getPermissionsHashMap().get(permission).getName());
            builder.setNeutralButton("Zamknij", null);
            builder.create().show();
        }
    }
}
