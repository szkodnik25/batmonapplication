package wmii.batmon.applicationinfo;

import android.Manifest;
import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import wmii.applicationscanner.Application;
import wmii.batmon.R;

public class PermissionsListBuilderImpl implements PermissionsListBuilder {

    HashMap<String, PermissionWithDescription> permissionsHashMap;
    Context context;

    public PermissionsListBuilderImpl(Context context) {
        this.context = context;
        this.permissionsHashMap = new HashMap<String, PermissionWithDescription>();

        PermissionWithDescription coarseLocation = new PermissionWithDescription(   context.getResources().getString(R.string.ACCESS_COARSE_LOCATION_title),
                                                                                    context.getResources().getString(R.string.ACCESS_COARSE_LOCATION_desc));
        PermissionWithDescription fineLocation = new PermissionWithDescription( context.getResources().getString(R.string.ACCESS_FINE_LOCATION_title),
                                                                                context.getResources().getString(R.string.ACCESS_FINE_LOCATION_desc));
        PermissionWithDescription callPhone = new PermissionWithDescription(context.getResources().getString(R.string.CALL_PHONE_title),
                                                                            context.getResources().getString(R.string.CALL_PHONE_desc));
        PermissionWithDescription camera = new PermissionWithDescription(   context.getResources().getString(R.string.CAMERA_title),
                                                                            context.getResources().getString(R.string.CAMERA_desc));
        PermissionWithDescription changeNetworkState = new PermissionWithDescription(   context.getResources().getString(R.string.CHANGE_NETWORK_STATE_title),
                                                                                        context.getResources().getString(R.string.CHANGE_NETWORK_STATE_desc));
        PermissionWithDescription clearAppCache = new PermissionWithDescription(context.getResources().getString(R.string.CLEAR_APP_CACHE_title),
                                                                                context.getResources().getString(R.string.CLEAR_APP_CACHE_desc));
        PermissionWithDescription internet = new PermissionWithDescription( context.getResources().getString(R.string.INTERNET_title),
                                                                            context.getResources().getString(R.string.INTERNET_desc));
        PermissionWithDescription modifyPhoneState = new PermissionWithDescription( context.getResources().getString(R.string.MODIFY_PHONE_STATE_title),
                                                                                    context.getResources().getString(R.string.MODIFY_PHONE_STATE_desc));
        PermissionWithDescription nfc = new PermissionWithDescription(  context.getResources().getString(R.string.NFC_title),
                                                                        context.getResources().getString(R.string.NFC_desc));
        PermissionWithDescription wakeLock = new PermissionWithDescription( context.getResources().getString(R.string.WAKE_LOCK_title),
                                                                            context.getResources().getString(R.string.WAKE_LOCK_desc));
        PermissionWithDescription vibrate = new PermissionWithDescription(  context.getResources().getString(R.string.VIBRATE_title),
                                                                            context.getResources().getString(R.string.VIBRATE_desc));
        permissionsHashMap.put(Manifest.permission.ACCESS_COARSE_LOCATION, coarseLocation);
        permissionsHashMap.put(Manifest.permission.ACCESS_FINE_LOCATION, fineLocation);
        permissionsHashMap.put(Manifest.permission.CALL_PHONE, callPhone);
        //permissionsHashMap.put(Manifest.permission.CAPTURE_AUDIO_OUTPUT, context.getResources().getString(R.string.CAPTURE_AUDIO_OUTPUT_title));
        permissionsHashMap.put(Manifest.permission.CAMERA, camera);
        permissionsHashMap.put(Manifest.permission.CHANGE_NETWORK_STATE, changeNetworkState);
        permissionsHashMap.put(Manifest.permission.CLEAR_APP_CACHE, clearAppCache);
        permissionsHashMap.put(Manifest.permission.INTERNET, internet);
        permissionsHashMap.put(Manifest.permission.MODIFY_PHONE_STATE, modifyPhoneState);
        permissionsHashMap.put(Manifest.permission.NFC, nfc);
        permissionsHashMap.put(Manifest.permission.WAKE_LOCK, wakeLock);
        permissionsHashMap.put(Manifest.permission.VIBRATE, vibrate);
    }

    @Override
    public List<String> buildCommonPermissionsList(Application application) {
        List<String> commonPermissionsList = new ArrayList<>();

        for (String permission : application.getPermissions()) {
            if (permissionsHashMap.containsKey(permission)) {
                commonPermissionsList.add(permissionsHashMap.get(permission).getName());
            }
        }

        return commonPermissionsList;
    }

    @Override
    public List<String> buildAllPermissionsList(Application application) {
        List<String> commonPermissionsList = new ArrayList<>();
        List<String> otherPermissions = new ArrayList<>();

        for (String permission : application.getPermissions()) {
            if (permissionsHashMap.containsKey(permission)) {
                commonPermissionsList.add(permissionsHashMap.get(permission).getName());
            } else {
                otherPermissions.add(permission);
            }
        }
        commonPermissionsList.addAll(otherPermissions);
        return commonPermissionsList;
    }

    @Override
    public HashMap<String, PermissionWithDescription> getPermissionsHashMap() {
        return permissionsHashMap;
    }
}
