package wmii.batmon.applicationinfo;

import java.util.HashMap;
import java.util.List;

import wmii.applicationscanner.Application;

public interface PermissionsListBuilder {
    List<String> buildCommonPermissionsList(Application application);
    List<String> buildAllPermissionsList(Application application);
    HashMap<String, PermissionWithDescription> getPermissionsHashMap();
}
