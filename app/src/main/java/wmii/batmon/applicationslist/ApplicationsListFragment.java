package wmii.batmon.applicationslist;

import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import wmii.applicationscanner.Application;
import wmii.applicationscanner.ApplicationScanner;
import wmii.applicationscanner.ApplicationScannerImpl;
import wmii.applicationscanner.CacheHandler;
import wmii.applicationscanner.DataUsageManager;
import wmii.batmon.R;
import wmii.batmon.online.OnlineManager;
import wmii.batmon.online.OnlineManagerImpl;
import wmii.batmon.preferences.PreferencesManager;
import wmii.batmon.preferences.SharedPreferencesManager;

public class ApplicationsListFragment extends ListFragment {

    List<Application> applications;
    ApplicationScanner scanner;
    ApplicationsListAdapter applicationsListAdapter;
    ApplicationClickedListener applicationClickedListener;
    TypeToken<List<Application>> applicationsListTypeToken;

    PreferencesManager preferencesManager;
    CacheHandler cacheHandler;
    Gson gson;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        preferencesManager = new SharedPreferencesManager(getActivity());
        preferencesManager.initializePreferences();
        new ApplicationLoader().execute();
        return inflater.inflate(R.layout.list_view_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        ApplicationsListAdapter adapter = (ApplicationsListAdapter) l.getAdapter();
        applicationClickedListener = (ApplicationClickedListener) getActivity();
        applicationClickedListener.onApplicationClicked(adapter.getItem(position));
    }

    public void refreshList() {
        new ApplicationsRefreshTask().execute();
    }

    private class ApplicationLoader extends AsyncTask<Void, Void, List<Application>> {
        ProgressDialog dialog = null;

        @Override
        protected List<Application> doInBackground(Void... params) {
            // TODO: 2015-12-10 rzuca Nullpointer (losowo)
            if (applications == null) {
                if (cacheHandler.doesCacheFileExist()) {
                    applications = gson.fromJson(cacheHandler.read(), applicationsListTypeToken.getType());
                } else {
                    applications = scanner.buildApplicaitonListWithDataUsage();
                    cacheHandler.write(gson.toJson(applications));
                }
                Collections.sort(applications);
            }
            return applications;
        }

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(getActivity(), "Pierwsze uruchomienie", "Budowanie listy aplikacji...");

            scanner = new ApplicationScannerImpl(getActivity());
            cacheHandler = new CacheHandler(getActivity());
            gson = new Gson();
            applicationsListTypeToken = new TypeToken<List<Application>>(){};
        }

        @Override
        protected void onPostExecute(List<Application> result) {
            if (result != null) {
                applicationsListAdapter = new ApplicationsListAdapter(getActivity(), R.layout.all_apps_list_element, result);
                setListAdapter(applicationsListAdapter);
            }
            dialog.dismiss();
        }
    }

    private class ApplicationsRefreshTask extends AsyncTask<Void, Void, Void> {

        ProgressDialog dialog = null;

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(getActivity(), "Odświeżam", "Odświeżam statystyki aplikacji");
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            dialog.dismiss();
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (cacheHandler.doesCacheFileExist()) {
                applications.clear();
                List<Application> applications = gson.fromJson(cacheHandler.read(), applicationsListTypeToken.getType());
                for (final Application application : applications) {
                    try {
                        final long mobileRxFg = DataUsageManager.getMobileRxForegroundData(application.getUid());
                        final long mobileRxBg = DataUsageManager.getMobileRxBackgroundData(application.getUid());

                        final long wlanRxFg = DataUsageManager.getWlanRxForegroundData(application.getUid());
                        final long wlanRxBg = DataUsageManager.getWlanRxBackgroundData(application.getUid());

                        if (preferencesManager.isOnlineModeEnabled()) {
                            UpdateUsageRunnable updateUsageRunnable = new UpdateUsageRunnable(application, mobileRxFg, mobileRxBg, wlanRxFg, wlanRxBg, preferencesManager.getWholeServerAddres());
                            Thread updateUsageThread = new Thread(updateUsageRunnable);
                            updateUsageThread.start();
                            updateUsageThread.join();
                        }
                        application.updateUsage(mobileRxFg, mobileRxBg, wlanRxFg, wlanRxBg);
                    } catch (IOException | InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                Collections.sort(applications);
                cacheHandler.write(gson.toJson(applications));
                ApplicationsListFragment.this.applications.addAll(applications);
            }

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    applicationsListAdapter.notifyDataSetChanged();
                }
            });

            return null;
        }
    }

    private class UpdateUsageRunnable implements Runnable {

        Application application;
        long mobileRxFg;
        long mobileRxBg;
        long wlanRxFg;
        long wlanRxBg;
        String URL;

        public UpdateUsageRunnable(Application application, long mobileRxFg, long mobileRxBg, long wlanRxFg, long wlanRxBg, String URL) {
            this.application = application;
            this.mobileRxFg = mobileRxFg;
            this.mobileRxBg = mobileRxBg;
            this.wlanRxFg = wlanRxFg;
            this.wlanRxBg = wlanRxBg;
            this.URL = URL;
        }

        @Override
        public void run() {
            if (!this.application.hasNullDataUsage()) {
                OnlineManager onlineManager = new OnlineManagerImpl(URL);
                onlineManager.updateUsageIfNecessary(application, mobileRxFg, mobileRxBg, wlanRxFg, wlanRxBg);
            }
        }
    }
}
