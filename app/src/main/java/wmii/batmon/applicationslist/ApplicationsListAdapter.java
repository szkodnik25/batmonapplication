package wmii.batmon.applicationslist;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import wmii.applicationscanner.Application;
import wmii.batmon.R;
import wmii.batmon.StringFormatter;

public class ApplicationsListAdapter extends ArrayAdapter<Application> {

    private Context context;
    private List<Application> applications;

    public ApplicationsListAdapter(Context context, int resource, List<Application> objects) {
        super(context, resource, objects);
        this.applications = objects;
        this.context = context;
    }

    @Override
    public Context getContext() {
        return super.getContext();
    }

    @Override
    public int getCount() {
        return applications.size();
    }

    @Override
    public Application getItem(int position) {
        return applications.get(position);
    }

    @Override
    public int getPosition(Application item) {
        return applications.indexOf(item);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.all_apps_list_element, null);
            holder = new ViewHolder();

            holder.nameTextView = (TextView) convertView.findViewById(R.id.application_name);
            holder.iconImageView = (ImageView) convertView.findViewById(R.id.app_icon);

            holder.downloadedMobileData = (TextView) convertView.findViewById(R.id.downloaded_mobile_data);
            holder.downloadedWlanData = (TextView) convertView.findViewById(R.id.downloaded_wlan_data);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Application app = applications.get(position);

        holder.nameTextView.setText(app.getName());

        String mobilePrefix = context.getResources().getString(R.string.mobile_data);
        String wlanPrefix = context.getResources().getString(R.string.wlan_data);
        String mobile = StringFormatter.formatLongToStringWithDataUnit(app.getMobileRxFg());
        String wlan = StringFormatter.formatLongToStringWithDataUnit(app.getWlanRxFg());

        holder.downloadedMobileData.setText(String.format("%s %s", mobilePrefix, mobile));
        holder.downloadedWlanData.setText(String.format("%s %s", wlanPrefix, wlan));

        try {
            Drawable icon = context.getPackageManager().getApplicationIcon(app.get_id());
            holder.iconImageView.setImageDrawable(icon);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return convertView;
    }

    private class ViewHolder {
        private TextView nameTextView;
        private ImageView iconImageView;
        private TextView downloadedMobileData;
        private TextView downloadedWlanData;
    }
}
