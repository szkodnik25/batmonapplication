package wmii.batmon.applicationslist;

import wmii.applicationscanner.Application;

public interface ApplicationClickedListener {
    public void onApplicationClicked(Application application);
}
