package wmii.batmon.mainscreen;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import wmii.batmon.R;

public class DrawerListAdapter extends ArrayAdapter<DrawerListElement> {

    private List<DrawerListElement> drawerListElements;
    private Context context;
    private int resource;

    public DrawerListAdapter(Context context, int resource, List<DrawerListElement> objects) {
        super(context, resource, objects);
        this.context = context;
        this.drawerListElements = objects;
        this.resource = resource;
    }

    @Override
    public int getCount() {
        return drawerListElements.size();
    }

    @Override
    public DrawerListElement getItem(int position) {
        return drawerListElements.get(position);
    }

    @Override
    public int getPosition(DrawerListElement item) {
        return drawerListElements.indexOf(item);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(resource, null);

            holder = new ViewHolder();
            holder.drawerOptionIcon = (ImageView) convertView.findViewById(R.id.drawer_list_element_icon);
            holder.drawerOptionText = (TextView) convertView.findViewById(R.id.drawer_list_element_text);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        DrawerListElement element = drawerListElements.get(position);
        holder.drawerOptionIcon.setImageDrawable(element.getIcon());
        holder.drawerOptionText.setText(element.getText());

        return convertView;
    }

    private class ViewHolder {
        private ImageView drawerOptionIcon;
        private TextView drawerOptionText;
    }
}
