package wmii.batmon.mainscreen;

import android.graphics.drawable.Drawable;

public class DrawerListElement {
    private Drawable icon;
    private String text;

    public DrawerListElement(Drawable icon, String text) {
        this.icon = icon;
        this.text = text;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
