package wmii.batmon.mainscreen;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;

import wmii.batmon.R;
import wmii.batmon.preferences.SharedPreferencesManager;

public class ChangeIpDialog extends DialogFragment {

    View view;
    SharedPreferencesManager sharedPreferencesManager;
    private NumberPicker firstPicker;
    private NumberPicker secondPicker;
    private NumberPicker thirdPicker;
    private NumberPicker fourthPicker;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        sharedPreferencesManager = new SharedPreferencesManager(getActivity());
        sharedPreferencesManager.initializePreferences();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        view = inflater.inflate(R.layout.change_ip_dialog_layout, null);
        initNumberPickers();

        builder.setView(view);
        builder.setTitle("Adres IP serwera");
        builder.setPositiveButton("Akceptuj", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String address = String.format("%s.%s.%s.%s", String.valueOf(firstPicker.getValue()), String.valueOf(secondPicker.getValue()),
                        String.valueOf(thirdPicker.getValue()), String.valueOf(fourthPicker.getValue()));

                sharedPreferencesManager.setServerIp(address);
            }
        });
        builder.setNegativeButton("Anuluj", null);
        return builder.create();
    }

    private void initNumberPickers() {
        firstPicker = (NumberPicker) view.findViewById(R.id.first_picker);
        secondPicker = (NumberPicker) view.findViewById(R.id.second_picker);
        thirdPicker = (NumberPicker) view.findViewById(R.id.third_picker);
        fourthPicker = (NumberPicker) view.findViewById(R.id.fourth_picker);

        firstPicker.setMinValue(0);
        firstPicker.setMaxValue(255);

        secondPicker.setMinValue(0);
        secondPicker.setMaxValue(255);

        thirdPicker.setMinValue(0);
        thirdPicker.setMaxValue(255);

        fourthPicker.setMinValue(0);
        fourthPicker.setMaxValue(255);

        String address = sharedPreferencesManager.getServerIp();
        String[] digits = address.split("\\.");
        firstPicker.setValue(Integer.valueOf(digits[0]));
        secondPicker.setValue(Integer.valueOf(digits[1]));
        thirdPicker.setValue(Integer.valueOf(digits[2]));
        fourthPicker.setValue(Integer.valueOf(digits[3]));
    }

}
