package wmii.batmon.mainscreen;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.jar.Manifest;

import wmii.applicationscanner.Application;
import wmii.batmon.applicationslist.ApplicationClickedListener;
import wmii.batmon.R;
import wmii.batmon.applicationinfo.ApplicationInfoFragment;
import wmii.batmon.applicationslist.ApplicationsListFragment;
import wmii.batmon.online.OnlineApplication;
import wmii.batmon.online.OnlineListFragment;
import wmii.batmon.online.OnlineManager;
import wmii.batmon.online.OnlineManagerImpl;
import wmii.batmon.preferences.PreferencesManager;
import wmii.batmon.preferences.SharedPreferencesManager;
import wmii.batmon.settingsscreen.SettingsFragment;
import wmii.restclient.BatMonRestClient;

public class MainActivity extends AppCompatActivity implements ApplicationClickedListener {

    private static final String TAG = "APP";

    private PreferencesManager preferencesManager;

    private FragmentManager fragmentManager;
    private ActionBar actionBar;
    private ApplicationsListFragment applicationsListFragment;

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private String activityTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentManager = getFragmentManager();
        applicationsListFragment = new ApplicationsListFragment();
        activityTitle = getTitle().toString();

        preferencesManager = new SharedPreferencesManager(this);
        preferencesManager.initializePreferences();
        initActionBar();
        initDrawerLayout();
        initDrawerList();
        setupDrawer();
        addInitialListFragment();
        //startService(new Intent(this, ApplicationScannerService.class));
    }

    private void initDrawerLayout() {
        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
    }

    private void initActionBar() {
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
    }

    private void addInitialListFragment() {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.fragment_container, applicationsListFragment);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commit();
    }

    private void initDrawerList() {
        final ListView drawerList = (ListView) findViewById(R.id.drawer_list);
        final List<DrawerListElement> drawerListElements = new ArrayList<>();

        Drawable appsIcon = ContextCompat.getDrawable(this, R.drawable.ic_drawer_apps);
        Drawable onlineIcon = ContextCompat.getDrawable(this, R.drawable.ic_drawer_server);
        Drawable settingsIcon = ContextCompat.getDrawable(this, R.drawable.ic_drawer_settings);
        Drawable exitIcon = ContextCompat.getDrawable(this, R.drawable.ic_drawer_exit);

        final DrawerListElement myApplications = new DrawerListElement(appsIcon, getResources().getString(R.string.drawer_my_apps));
        final DrawerListElement online = new DrawerListElement(onlineIcon, getResources().getString(R.string.drawer_online));
        final DrawerListElement settings = new DrawerListElement(settingsIcon, getResources().getString(R.string.drawer_settings));
        final DrawerListElement exit = new DrawerListElement(exitIcon, getResources().getString(R.string.drawer_exit));

        drawerListElements.add(myApplications);
        drawerListElements.add(online);
        drawerListElements.add(settings);
        drawerListElements.add(exit);

        DrawerListAdapter drawerListAdapter = new DrawerListAdapter(this, R.layout.drawer_list_element, drawerListElements);
        drawerList.setAdapter(drawerListAdapter);
        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (drawerListElements.get(position).equals(myApplications)) {
                    drawerLayout.closeDrawers();
                    Fragment currentlyVisible = fragmentManager.findFragmentById(R.id.fragment_container);
                    if (!(currentlyVisible instanceof ApplicationsListFragment)) {
                        FragmentTransaction transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.fragment_container, applicationsListFragment);
                        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                        transaction.commit();
                    }
                }

                if (drawerListElements.get(position).equals(online)) {
                    if (preferencesManager.isOnlineModeEnabled()) {
                        drawerLayout.closeDrawers();
                        FragmentTransaction transaction = fragmentManager.beginTransaction();
                        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                        transaction.replace(R.id.fragment_container, new OnlineListFragment());
                        transaction.commit();
                        actionBar.setSubtitle("Aplikacje Online");
                    } else {
                        Toast.makeText(MainActivity.this, "Tryb Online nieaktywny.", Toast.LENGTH_SHORT).show();
                    }
                }

                if (drawerListElements.get(position).equals(settings)) {
                    drawerLayout.closeDrawers();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    transaction.replace(R.id.fragment_container, new SettingsFragment());
                    transaction.commit();
                }

                if (drawerListElements.get(position).equals(exit)) {
                    MainActivity.this.finish();
                }
            }
        });
    }

    private void setupDrawer() {
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                actionBar.setTitle("Menu");
                actionBar.setSubtitle("");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                actionBar.setTitle(activityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mainmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_change_ip) {
            new ChangeIpDialog().show(fragmentManager, "change_ip_dialog");
            return true;
        }

        if (id == R.id.action_exit) {
            this.finish();
            return true;
        }

        if (id == R.id.action_refresh) {
            // refresh
            Toast.makeText(this, "Odświeżam...", Toast.LENGTH_SHORT).show();
            applicationsListFragment.refreshList();
            return true;
        }

        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onApplicationClicked(Application application) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_down);
        transaction.replace(R.id.fragment_container, ApplicationInfoFragment.newInstance(application), TAG);
        // Subtelniejsza animacja
        //transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commit();

        actionBar.setTitle(application.getName());
        actionBar.setSubtitle("Szczegóły");
    }

    @Override
    public void onBackPressed() {
        Fragment fragmentToReplace = getFragmentManager().findFragmentByTag(TAG);

        if (fragmentToReplace == null) {
            this.finish();
        } else {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_down);
            transaction.replace(R.id.fragment_container, applicationsListFragment);
            transaction.commit();
        }
    }
}
