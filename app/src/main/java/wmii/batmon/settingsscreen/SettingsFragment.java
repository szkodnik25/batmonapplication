package wmii.batmon.settingsscreen;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import wmii.batmon.R;
import wmii.batmon.preferences.PreferencesManager;
import wmii.batmon.preferences.SharedPreferencesManager;

public class SettingsFragment extends Fragment {

    PreferencesManager preferencesManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        preferencesManager = new SharedPreferencesManager(getActivity());
        preferencesManager.initializePreferences();

        View view = inflater.inflate(R.layout.settings_fragment, container, false);
        Switch onlineSwitch = (Switch) view.findViewById(R.id.online_mode_switch);
        onlineSwitch.setOnCheckedChangeListener(onlineModeSwitchListener());

        if (preferencesManager.isOnlineModeEnabled()) {
            onlineSwitch.setChecked(true);
        }
        return view;
    }

    private CompoundButton.OnCheckedChangeListener onlineModeSwitchListener() {
        return new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    preferencesManager.setOnlineMode(true);
                } else {
                    preferencesManager.setOnlineMode(false);
                }
            }
        };
    }
}
