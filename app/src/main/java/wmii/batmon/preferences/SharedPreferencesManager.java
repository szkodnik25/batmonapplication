package wmii.batmon.preferences;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesManager implements PreferencesManager {

    Context context;
    SharedPreferences onlineModePreference;
    SharedPreferences firstRunPreference;
    SharedPreferences serverIpPreference;

    public SharedPreferencesManager(Context context) {
        this.context = context;
    }

    @Override
    public void initializePreferences() {
        onlineModePreference = context.getSharedPreferences(PreferencesManager.ONLINE_PREFERENCE_NAME, Context.MODE_PRIVATE);
        firstRunPreference = context.getSharedPreferences(PreferencesManager.FIRST_RUN_PREFERENCE_NAME, Context.MODE_PRIVATE);
        serverIpPreference = context.getSharedPreferences(PreferencesManager.SERVER_IP_PREFERENCE_NAME, Context.MODE_PRIVATE);

        if (!firstRunPreference.contains(PreferencesManager.FIRST_RUN_KEY)) {
            SharedPreferences.Editor firstRunEditor = firstRunPreference.edit();
            firstRunEditor.putInt(PreferencesManager.FIRST_RUN_KEY, PreferencesManager.FIRST_RUN_DEFAULT);
            firstRunEditor.apply();
        }

        if (!onlineModePreference.contains(PreferencesManager.ONLINE_MODE_KEY)) {
            SharedPreferences.Editor onlineModeEditor = onlineModePreference.edit();
            onlineModeEditor.putInt(PreferencesManager.ONLINE_MODE_KEY, PreferencesManager.ONLINE_MODE_DEFAULT);
            onlineModeEditor.apply();
        }

        if (!serverIpPreference.contains(PreferencesManager.SERVER_IP_KEY)) {
            SharedPreferences.Editor serverIpEditor = serverIpPreference.edit();
            serverIpEditor.putString(PreferencesManager.SERVER_IP_KEY, PreferencesManager.SERVER_IP_DEFAULT);
            serverIpEditor.apply();
        }
    }

    @Override
    public boolean isOnlineModeEnabled() {
        return onlineModePreference.getInt(PreferencesManager.ONLINE_MODE_KEY, PreferencesManager.ONLINE_MODE_DEFAULT) == 1;
    }

    @Override
    public boolean isItFirstRun() {
        return firstRunPreference.getInt(PreferencesManager.FIRST_RUN_KEY, PreferencesManager.FIRST_RUN_DEFAULT) == 1;
    }

    @Override
    public String getServerIp() {
        return serverIpPreference.getString(PreferencesManager.SERVER_IP_KEY, PreferencesManager.SERVER_IP_DEFAULT);
    }

    @Override
    public String getWholeServerAddres() {
        return String.format("%s", "http://" + serverIpPreference.getString(PreferencesManager.SERVER_IP_KEY, PreferencesManager.SERVER_IP_DEFAULT) + ":8080/");
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    public void setOnlineMode(boolean enabled) {
        SharedPreferences.Editor editor = onlineModePreference.edit();
        if (enabled) {
            editor.putInt(PreferencesManager.ONLINE_MODE_KEY, 1);
        } else {
            editor.putInt(PreferencesManager.ONLINE_MODE_KEY, 0);
        }
        editor.commit();
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    public void setFirstRun(boolean enabled) {
        SharedPreferences.Editor editor = firstRunPreference.edit();
        if (enabled) {
            editor.putInt(PreferencesManager.FIRST_RUN_KEY, 1);
        } else {
            editor.putInt(PreferencesManager.FIRST_RUN_KEY, 0);
        }
        editor.commit();
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    public void setServerIp(String address) {
        SharedPreferences.Editor editor = serverIpPreference.edit();
        editor.putString(PreferencesManager.SERVER_IP_KEY, address);
        editor.commit();
    }
}
