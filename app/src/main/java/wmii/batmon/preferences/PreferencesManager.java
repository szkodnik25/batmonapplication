package wmii.batmon.preferences;

public interface PreferencesManager {

    String ONLINE_PREFERENCE_NAME = "batmon.ONLINE";
    String FIRST_RUN_PREFERENCE_NAME = "batmon.FIRST_RUN";
    String SERVER_IP_PREFERENCE_NAME = "batmon.SERVER_IP";

    String ONLINE_MODE_KEY = "online_mode";
    String FIRST_RUN_KEY = "first_run";
    String SERVER_IP_KEY = "server_ip";

    int ONLINE_MODE_DEFAULT = 1;
    int FIRST_RUN_DEFAULT = 1;
    String SERVER_IP_DEFAULT = "192.168.0.100";

    void initializePreferences();

    boolean isOnlineModeEnabled();
    boolean isItFirstRun();
    String getServerIp();
    String getWholeServerAddres();

    void setOnlineMode(boolean enabled);
    void setFirstRun(boolean enabled);
    void setServerIp(String address);
}
